jQuery.noConflict();
jQuery(document).ready(function($){
	$('body').mousemove(function(event) {
		$('.header img').css({'left':event.pageX,'bottom':event.pageY/40});
		$('.content .shape_left,.content .shape_right,.content .close').css({'left':event.pageX});
		$('.footer .left img').css({'left':event.pageX/40,'bottom':-event.pageY/40});
		$('.footer .right img').css({'left':-event.pageX/90,'bottom':-event.pageY/90});
	});
	$('.content .close').on('click',function(){
		$('body').addClass('content_off');
	});
});